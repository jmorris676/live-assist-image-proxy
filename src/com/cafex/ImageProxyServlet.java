package com.cafex;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.codec.binary.Base64;
import org.apache.tomcat.util.codec.binary.StringUtils;

@WebServlet("/ImageProxyServlet")
public class ImageProxyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public ImageProxyServlet() {
        super();
    }
    
    private static byte[] isToByteArray(InputStream inputStream) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();				
		byte[] buffer = new byte[1024];
		int read = 0;
		try {
			while ((read = inputStream.read(buffer, 0, buffer.length)) != -1) {
				baos.write(buffer, 0, read);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}		
		try {
			baos.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return baos.toByteArray();
    }
    
    private String getJSONPResponse(InputStream inputStream, String callbackName, String responseType) {
    	String jsonPaddedResponse = callbackName + "({content: '";
    	
    	byte[] content = isToByteArray(inputStream);
    	
    	jsonPaddedResponse = jsonPaddedResponse.concat(StringUtils.newStringUtf8(Base64.encodeBase64(content, false)));
    	jsonPaddedResponse = jsonPaddedResponse.concat("'," + " type: '" + responseType + "' })");
    	
    	return jsonPaddedResponse;
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Map<String, String> urlParams = getQueryMap(request.getQueryString());
		String url = urlParams.get("url");
		
		if (url != null) {
			url = URLDecoder.decode(url, "UTF-8");
		}
		
		HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
		String callback = urlParams.get("callback");

		if (callback == null) {
			InputStream httpResponse = connection.getInputStream();
			//Proxy it:
			Map<String, List<String>> map = connection.getHeaderFields();
			for (Map.Entry<String, List<String>> entry : map.entrySet()) {
				if (entry.getKey() != null) {
					response.setHeader(entry.getKey(), entry.getValue().get(0));
				}
			}
			
			OutputStream outputStream = response.getOutputStream();
			
			byte[] buffer = new byte[4096];
			int n = - 1;

			while ( (n = httpResponse.read(buffer)) != -1) 
			{
				outputStream.write(buffer, 0, n);
			}
			outputStream.close();
		}
		else {
			if (connection.getResponseCode() != 200) {
				String jsonPError = callback + "({'error': 'Request failed', 'code': " + connection.getResponseCode() 
				+ ", 'message': '" + connection.getResponseMessage() + "', 'url': '" + url + "'});";
				response.getWriter().println(jsonPError);
				return;
			}
			
			InputStream httpResponse = connection.getInputStream();
			
			//jsonp it:
			String JSONPResponse = getJSONPResponse(httpResponse, callback, connection.getHeaderField("content-type"));
			response.setHeader("content-type", "application/javascript");
			response.getWriter().print(JSONPResponse);
		}		
	}
	
	public static Map<String, String> getQueryMap(String query)  
	{  
	    String[] params = query.split("&");  
	    Map<String, String> map = new HashMap<String, String>();  
	    for (String param : params)  
	    {  
	        String name = param.split("=")[0];  
	        String value = param.split("=")[1];  
	        map.put(name, value);  
	    }  
	    return map;  
	}

}
