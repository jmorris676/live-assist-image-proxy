# README #

A quickly written sample proxy for [html2canvas](https://github.com/niklasvh/html2canvas) images.
Primarily written to get around [access control of resources on the web](https://developer.mozilla.org/en/docs/Web/HTTP/Access_control_CORS) 
Could also be used to proxy documents for document-sharing

### How do I get set up? ###

Add [tomcat-util](https://mvnrepository.com/artifact/org.apache.tomcat/tomcat-util/8.5.4) to classpath and build as WAR to add to AS.